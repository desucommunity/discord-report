#pragma semicolon 1

#include <sourcemod>
#include <multicolors>
#include <regex>
#include <SteamWorks>
#include <autoexecconfig>
#include <discord>

#pragma newdecls required

char ServerIP[30];

ConVar g_hCooldown = null;
ConVar g_hMinlen = null;

//
bool bInReason[MAXPLAYERS + 1];
int iTargetCache[MAXPLAYERS + 1];
float fNextUse[MAXPLAYERS + 1];
//

ConVar g_cWebhook = null;
ConVar g_cAvatar = null;
ConVar g_cUsername = null;
ConVar g_cColor = null;
ConVar g_ServerName = null;

#define TAG_PREFIX "[{orange}Desu{default}] "

public Plugin myinfo =
{
	name = "Report",
	author = "Trayz",
	description = "Adds a report to a database",
	version = "1.0"
};

public void OnPluginStart()
{
	RegConsoleCmd("sm_report", Command_Report);
	RegConsoleCmd("sm_denunciar", Command_Report);

	AutoExecConfig_SetCreateDirectory(true);
	AutoExecConfig_SetCreateFile(true);
	AutoExecConfig_SetFile("discord.report");
	g_cWebhook = AutoExecConfig_CreateConVar("discord_report_notification_webhook", "ReportNotification", "Discord webhook name for this plugin (addons/sourcemod/configs/Discord.cfg)");
	g_cAvatar = AutoExecConfig_CreateConVar("discord_report_notification_avatar", "https://desucm.tk/avatar.png", "URL to Avatar image");
	g_cUsername = AutoExecConfig_CreateConVar("discord_report_notification_username", "DesuCommunity", "Discord username");
	g_ServerName = AutoExecConfig_CreateConVar("discord_report_servername", "Courses", "Server Name");
	g_cColor = AutoExecConfig_CreateConVar("discord_report_notification_color", "#FF0000", "Hexcode of the color (with '#' !)");
	g_hCooldown = AutoExecConfig_CreateConVar("discord_report_cooldown", "60.0", "Cooldown in seconds between per report per user");
	g_hMinlen = AutoExecConfig_CreateConVar("discord_report_minlen", "5", "Minimum reason length");
	AutoExecConfig_ExecuteFile();
	AutoExecConfig_CleanFile();

	LoadTranslations("discord_report.phrases");
	
	GetServerIP(ServerIP, sizeof(ServerIP));
}

public Action Command_Report(int iClient, int iArgs)
{
	if (!IsValidClient(iClient))
		return Plugin_Handled;

	if (OnCooldown(iClient))
	{
		CPrintToChat(iClient, "%s%T", TAG_PREFIX, "In Cooldown", iClient, GetRemainingTime(iClient));

		return Plugin_Handled;
	}

	Menu PList = new Menu(ReportMenu);

	char sName[MAX_NAME_LENGTH], sIndex[4];

	for (int i = 0; i <= MaxClients; i++)
	{
		if (!IsValidClient(i))
			continue;

		if (CheckCommandAccess(i, "Staff", ADMFLAG_BAN, true))
			continue;
		
		if(i == iClient)
			continue;

		GetClientName(i, sName, sizeof sName);
		IntToString(i, sIndex, sizeof sIndex);

		PList.AddItem(sIndex, sName);
	}

	PList.Display(iClient, MENU_TIME_FOREVER);

	return Plugin_Handled;
}

public int ReportMenu(Menu menu, MenuAction action, int iClient, int iItem)
{
	switch (action)
	{
		case MenuAction_Select:
		{
			char sIndex[4];

			menu.GetItem(iItem, sIndex, sizeof sIndex);

			iTargetCache[iClient] = StringToInt(sIndex);

			bInReason[iClient] = true;

			CPrintToChat(iClient, "%s%T", TAG_PREFIX, "Reason Prompt", iClient);
		}
		case MenuAction_End:
			delete menu;
	}
}

public Action OnClientSayCommand(int iClient, const char[] sCommand, const char[] sArgs)
{
	if (!bInReason[iClient])
		return Plugin_Continue;

	if (!IsValidClient(iClient) || (iTargetCache[iClient] != -1 && !IsValidClient(iTargetCache[iClient])))
	{
		ResetInReason(iClient);

		return Plugin_Continue;
	}

	if (StrEqual(sArgs, "cancel", false))
	{
		CPrintToChat(iClient, "%s%T", TAG_PREFIX, "Report Canceled", iClient);

		ResetInReason(iClient);

		return Plugin_Stop;
	}

	if (strlen(sArgs) < g_hMinlen.IntValue)
	{
		CPrintToChat(iClient, "%s%T", TAG_PREFIX, "Reason Short Need More", iClient);

		return Plugin_Stop;
	}

	ReportPlayer(iClient, iTargetCache[iClient], sArgs);

	AddCooldown(iClient);

	ResetInReason(iClient);

	return Plugin_Stop;
}

public void ReportPlayer(int iReporter, int iTarget, const char[] sReason)
{
	if (!IsValidClient(iReporter))
		return;

	char SuspectID[32];
	GetSteamID(iTarget, SuspectID, sizeof(SuspectID));

	char SuspectName[MAX_NAME_LENGTH];
	GetClientName(iTarget, SuspectName, sizeof(SuspectName));

	char ReporterID[32];
	GetSteamID(iReporter, ReporterID, sizeof(ReporterID));

	char ReporterName[MAX_NAME_LENGTH];
	GetClientName(iReporter, ReporterName, sizeof(ReporterName));

	/* Get avatar url */
	char sAvatar[256];
	g_cAvatar.GetString(sAvatar, sizeof(sAvatar));

	char sTitle[50];
	g_ServerName.GetString(sTitle, sizeof(sTitle));

	/* Start and Send discord notification */
	char sWeb[256], sHook[256];
	g_cWebhook.GetString(sWeb, sizeof(sWeb));
	
	if (!GetWebHook(sWeb, sHook, sizeof(sHook)))
	{
		SetFailState("[Report Notification] (Timer_SendMessage) Can't find webhook");
		return;
	}

	DiscordWebHook hook = new DiscordWebHook(sHook);
	hook.SlackMode = true;

	char sName[128], sColor[8];
	g_cUsername.GetString(sName, sizeof(sName));
	g_cColor.GetString(sColor, sizeof(sColor));
	hook.SetUsername(sName);

	MessageEmbed Embed = new MessageEmbed();
	Embed.SetColor(sColor);
	Embed.SetTitle(sTitle);
	Embed.AddField("Denunciante:", ReporterName, true);
	Embed.AddField("ID:", ReporterID, true);
	Embed.AddField("Suspeito:", SuspectName, true);
	Embed.AddField("ID:", SuspectID, true);
	Embed.AddField("Razão:", sReason, true);
	hook.Embed(Embed);
	hook.Send();
	delete hook;

	CPrintToChat(iReporter, "%s%T", TAG_PREFIX, "Report Sent", iReporter);
}

void ResetInReason(int iClient)
{
	bInReason[iClient] = false;
	iTargetCache[iClient] = -1;
}

void AddCooldown(int iClient)
{
	fNextUse[iClient] = GetGameTime() + g_hCooldown.FloatValue;
}

bool OnCooldown(int iClient)
{
	return (fNextUse[iClient] - GetGameTime()) > 0.0;
}

float GetRemainingTime(int iClient)
{
	float fOffset = fNextUse[iClient] - GetGameTime();

	if (fOffset > 0.0)
		return fOffset;
	else
		return 0.0;
}

stock void GetSteamID(int client, char[] buffer, int len)
{
	GetClientAuthId(client, AuthId_Steam2, buffer, len);
	ReplaceString(buffer, len, "STEAM_1", "STEAM_0");
}

stock void GetServerIP(char[] buffer, int len)
{
	int ips[4];
	int ip = GetConVarInt(FindConVar("hostip"));
	int port = GetConVarInt(FindConVar("hostport"));
	ips[0] = (ip >> 24) & 0x000000FF;
	ips[1] = (ip >> 16) & 0x000000FF;
	ips[2] = (ip >> 8) & 0x000000FF;
	ips[3] = ip & 0x000000FF;
	Format(buffer, len, "%d.%d.%d.%d:%d", ips[0], ips[1], ips[2], ips[3], port);
}

stock bool IsValidClient(int iClient, bool bAlive = false)
{
	if (iClient >= 1 &&
		iClient <= MaxClients &&
		IsClientConnected(iClient) &&
		IsClientInGame(iClient) &&
		!IsFakeClient(iClient) &&
		(bAlive == false || IsPlayerAlive(iClient)))
	{
		return true;
	}

	return false;
}

bool GetWebHook(const char[] sWebhook, char[] sUrl, int iLength)
{
	KeyValues kvWebhook = new KeyValues("Discord");

	char sFile[PLATFORM_MAX_PATH];
	BuildPath(Path_SM, sFile, sizeof(sFile), "configs/Discord.cfg");

	if (!FileExists(sFile))
	{
		SetFailState("[Map Notification] (GetWebHook) \"%s\" not found!", sFile);
		delete kvWebhook;
		return false;
	}

	if (!kvWebhook.ImportFromFile(sFile))
	{
		SetFailState("[Map Notification] (GetWebHook) Can't read: \"%s\"!", sFile);
		delete kvWebhook;
		return false;
	}

	kvWebhook.GetString(sWebhook, sUrl, iLength, "default");

	if (strlen(sUrl) > 2)
	{
		delete kvWebhook;
		return true;
	}

	delete kvWebhook;
	return false;
}